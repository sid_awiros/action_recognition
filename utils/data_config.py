from utils import data_utils as utils

SCALING_FACTOR = 3
IMAGE_HEIGHT = 1920//SCALING_FACTOR
IMAGE_WIDTH  = 1080//SCALING_FACTOR
IMAGE_CHANNELS = 3
THICKNESS = 3
STEP_SIZE = 3

class PERSON:

    W, H = utils.scale_seq((173 , 590), SCALING_FACTOR)
    COLOR_CODE =  (255, 255, 255)


class LADDLE:

    W, H = utils.scale_seq((255, 400), SCALING_FACTOR)
    COLOR_CODE = (0, 0, 255)


class BUCKET:

    W, H = utils.scale_seq((75, 100), SCALING_FACTOR)
    COLOR_CODE =  (255, 0, 0)


class FUNNEL:

    W, H = utils.scale_seq((70, 240), SCALING_FACTOR)
    COLOR_CODE = (0, 255, 0)
