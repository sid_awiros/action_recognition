import matplotlib.pyplot as plt
import numpy as np
import cv2
import itertools

from utils import data_config as config
from utils import data_utils as utils


#funnel and person coming together
phase_1_data= []
person_dist = 50 #distance between person and funnel
init_x, intit_y = 0, config.IMAGE_HEIGHT//2# for PERSON
inti_xf, intit_yf = 0,  config.IMAGE_HEIGHT//2+  person_dist
seq_p= range(0, config.IMAGE_WIDTH//2 , 1*config.STEP_SIZE)
seq_f= range(person_dist, config.IMAGE_WIDTH//2 , 1*config.STEP_SIZE)
for j,i in itertools.izip(seq_f,seq_p):

    img_mask = np.zeros((config.IMAGE_HEIGHT,config.IMAGE_WIDTH , config.IMAGE_CHANNELS), np.uint8)
    curr_x, curr_y = i,  intit_y
    curr_xf, curr_yf = j, intit_yf
    x, y, w, h = utils.cornerify(curr_x, curr_y, config.PERSON.W, config.PERSON.H)
    x1,y1, w1,h1 =utils.cornerify(curr_xf,curr_yf,config.FUNNEL.W,config.FUNNEL.H)
    print(x, y)
    x,y= utils.noise(x,y)
    x1,y1= utils.noise(x1,y1)
    print(x,y)
    cv2.rectangle(img_mask, (x, y), (x + w, y + h), config.PERSON.COLOR_CODE, config.THICKNESS)
    cv2.rectangle(img_mask,(x1,y1),(x1+w1,y1+h1),config.FUNNEL.COLOR_CODE, config.THICKNESS)
    #cv2.imshow('phase-4_frame-{}'.format(i), img_mask)
    cv2.imshow("phase-4_fram", img_mask)
    k=cv2.waitKey(0)
