import matplotlib.pyplot as plt
import numpy as np
import cv2
import itertools

from utils import data_config as config
from utils import data_utils as utils


#phase 4 (Laddle lifting)
phase_4_data = []

init_x, intit_y = config.IMAGE_WIDTH//2, config.IMAGE_HEIGHT

for i in range(config.IMAGE_HEIGHT, config.IMAGE_HEIGHT//2 , -1*config.STEP_SIZE):

    img_mask = np.zeros((config.IMAGE_HEIGHT,config.IMAGE_WIDTH, config.IMAGE_CHANNELS), np.uint8)
    curr_x, curr_y = init_x, i
    x, y, w, h = utils.cornerify(curr_x, curr_y, config.LADDLE.W, config.LADDLE.H)
    print(x, y, w, h)
    cv2.rectangle(img_mask, (x, y), (x + w, y + h), config.LADDLE.COLOR_CODE, config.THICKNESS)
    #cv2.imshow('phase-4_frame-{}'.format(i), img_mask)
    cv2.imshow("phase-4_fram", img_mask)
    k=cv2.waitKey(0)
