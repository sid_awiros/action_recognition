import numpy as np
import random


def intify(arr):
    return [int(item) for item in arr]

def cornerify(x_center, y_center, w, h):
    return intify([x_center - w//2, y_center - h//2, w, h])

def scale_seq(pair, factor):
    return [item//factor for item in pair]

def noise(arx, ary):
    return (int(arx + np.random.normal(0.5,10)),int(ary + np.random.normal(0.5,10)))
